#### INTRODUCTION  

+ composer 安裝 `laravel/lumen`

		% composer create-project --prefer-dist laravel/lumen {PROJECT_FOLDER}  
		
+ `laravel/lumen` 的 **目錄結構**  

 		models						  => 'app'
		controller					  => 'app/Http/Controllers'
		設定檔							=> 'config'  
		外連檔(image/video/js/css)		=> 'public'
		core code					  => 'vendor'

+ `laravel/lumen` 的 **設定檔**

		基本設定			=> 'bootstrap/app.php'
		Route 路由設定		=> 'routes/web.php'
		環境設定			=> '.env'
		資料庫設定 		    => 'config/database.php'


#### vendor:publish for Lumen framework

`https://github.com/laravelista/lumen-vendor-publish` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/laravelista/lumen-vendor-publish)  

1. composer 安裝 `laravelista/lumen-vendor-publish`  
		
		% composer require laravelista/lumen-vendor-publish

2. 編輯 `app/Console/Kernel.php`

		# add
		#
		protected $commands = [
    		\Laravelista\LumenVendorPublish\VendorPublishCommand::class
		];


#### REPOSITORY DESIGN MODEL

`https://github.com/andersao/l5-repository` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/andersao/l5-repository)  

+ composer 安裝 `prettus/l5-repository`

		% composer require prettus/l5-repository  

+ 安裝 `prettus/l5-repository` 後的 **目錄結構**

		models			=> 'app/Entities'  
		repositories	=> 'app/Repositories'  
		interfaces		=> 'app/Repositories'  
		transformers	=> 'app/Transformers'  
		presenters		=> 'app/Presenters'  
		validators		=> 'app/Validators'  
		controllers		=> 'app/Http/Controllers'  
		provider		=> 'app/RepositoryServiceProvider'  
		criteria		=> 'app/Criteria'  


#### Laravel Snowflake   

This Laravel package to generate 64 bit identifier like the snowflake within Twitter. `https://github.com/kra8/laravel-snowflake` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/kra8/laravel-snowflake)

1. composer 安裝 `kra8/laravel-snowflake`  

		% composer require kra8/laravel-snowflake 

2. 編輯 `bootstrap/app.php`

		# add
		#
		'providers' => [
    		Kra8\Snowflake\SnowflakeServiceProvider::class,
		],

3. 新增 `config/snowflake.php`

		% php artisan vendor:publish 


#### RESTful API  

`How to Secure a REST API With Lumen` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://code.tutsplus.com/tutorials/how-to-secure-a-rest-api-with-lumen--cms-27442)  


#### AWS Service Provider for Laravel 5  

`https://github.com/aws/aws-sdk-php-laravel` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/aws/aws-sdk-php-laravel)  

1. composer 安裝 `aws/aws-sdk-php-laravel`  

		% composer require aws/aws-sdk-php-laravel 

2. 編輯 `bootstrap/app.php`

		# add
		#
		$app->register(Aws\Laravel\AwsServiceProvider::class);

3. 編輯 `config/aws.php`

		# add
		# 
		return [
    		'credentials' => [
       		'key'    => 'YOUR_AWS_ACCESS_KEY_ID',
        	'secret' => 'YOUR_AWS_SECRET_ACCESS_KEY',
    		],
    		'region' => 'us-west-2',
    		'version' => 'latest',
    
    		// You can override settings for specific services
    		'Ses' => [
       			'region' => 'us-east-1',
    		],
		];


#### Laravel Twillio API Integration

`https://github.com/aloha/laravel-twilio` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/aloha/laravel-twilio)  

1. composer 安裝 `aloha/twilio`  

		% composer require aloha/twilio 

2. 編輯 `config/twilio`，設定 sid 及 token

		return [
    		'twilio' => [
        		'default' => 'twilio',
        		'connections' => [
            		'twilio' => [

                		'sid' => getenv('TWILIO_SID') ?: '{SID}',
  
                		'token' => getenv('TWILIO_TOKEN') ?: '{TOKEN}',

                		'from' => getenv('TWILIO_FROM') ?: '',

                		'ssl_verify' => true,
            		],
        		],
    		],
		];



#### RESOURCES  

+ **Lumen**  
  官網 : `https://lumen.laravel.com` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://lumen.laravel.com)   
  簡中文件 : `https://lumen.laravel-china.org` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://lumen.laravel-china.org)   
  程式碼 : `https://github.com/laravel/lumen` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/laravel/lumen)   

+ **Laravel**  
  官網 : `https://laravel.com` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://laravel.com)  
  繁中官網 : `https://laravel.tw` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://laravel.tw)  
  簡中文件 : `https://d.laravel-china.org` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://d.laravel-china.org)  
  程式碼 : `https://github.com/laravel/laravel` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/laravel/laravel)  
  目錄結構 : `https://laravel.com/docs/5.5/structure` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://laravel.com/docs/5.5/structure)  
  新北市樹林國小 LARAVEL 工作坊 : `http://www.laravel-dojo.com/workshops/201507-ntpc` [![](https://png.icons8.com/external-link/win/16/2980b9)](http://www.laravel-dojo.com/workshops/201507-ntpc)  

