#### \# Something Before Using API

+ **SESSION**
> 1. 使用所有 api，必須在 Request 的 header 傳 `session`，來辨別是哪次傳輸。
> 2. 使用者登入的話，也會紀錄是使用哪個 `session`。
> 3. 目前預設 `session` 存活時間為 86400 秒 (24 hours)，所以當第一次或 `session` 已過期，須使用 `/session/init` 來產生及得到 `session`。

+ **SESSION on MOBILE**
> 1. app (移動裝置) 使用 `/session/init` 時，需傳入 `device_code` 來記錄哪個裝置產生 `session`。
> 2. 之後 AWS SNS 會用到 `device_code` 、 `device_token` 及 `device_channel` 傳訊息至 手機的訊息中心。

+ **TOKEN** 
> 1. 所有需要登入後才可使用的 api，需在 Request 的 header 傳 `token` 來識別使用者是否有權限使用該 api。
> 2. 使用 `/login` (登入) 之後，可得到 `token`。


#### \# BLOG

|  No.  | Route            | Method | Header Params     | Body Params           | Permission             | Description            |
|:-----:|------------------|:------:|-------------------|-----------------------|------------------------|------------------------|
|  01.  | /blog/{id}       |   GET  | session / [token] | id                    |                        | Find the blog          |
|  02.  | /blog/{id}/intro |   PUT  | session / token   | id / intro            | general / blog\_update | Change the intro.      |
|  03.  | /blog/{id}/cover |   PUT  | session / token   | id / cover / \_method | general / blog\_update | Upload the cover file. |


#### \# BOARD

|  No.  | Route                              | Method | Header Params     | Body Params                                                                               | Permission             | Description                              |
|:-----:|------------------------------------|:------:|-------------------|-------------------------------------------------------------------------------------------|------------------------|------------------------------------------|
|  01.  | /board                             |  POST  | session / token   | [parent\_type] / parent\_id / [privacy\_status] / [status] / content / [file\_ids]        | general / text\_create | Publish the text and files.              |
|  02.  | /board/{id}                        |   PUT  | session / token   | id / [parent\_type] / parent\_id / [privacy\_status] / [status] / [content] / [file\_ids] | general / text\_update | Update the text and files.               |
|  03.  | /board/{id}                        | DELETE | session / token   | id                                                                                        | general / text\_delete | Delete the text and files.               |
|  04.  | /board/{id}                        |   GET  | session / [token] | id                                                                                        |                        | find the text and files.                 |
|  05.  | /board/blog/{id}/{page}            |   GET  | session / [token] | id / [page]                                                                               |                        | find the text and files by the blog id.  |
|  06.  | /board/group/{id}/{page}           |   GET  | session / [token] | id / [page]                                                                               |                        | find the text and files by the group id. |
|  07.  | /board/{id}/view/count             |   PUT  | session / [token] | id                                                                                        |                        | count the view number with board.        |
|  08.  | /board/group/statistic/view/{page} |   GET  | session / token   | page                                                                                      | group / report\_read   | find the view statistic in group.        |


#### \# COMMENT

|  No.  | Route                        | Method | Header Params     | Body Params                                         | Permission                | Description                          |
|:-----:|------------------------------|:------:|-------------------|-----------------------------------------------------|---------------------------|--------------------------------------|
|  01.  | /comment                     |  POST  | session / token   | [parent\_type] / parent\_id / content / [file\_ids] | general / comment\_create | Create the comment.                  |
|  02.  | /comment/{id}                |   PUT  | session / token   | id / [status] / [content] / [file\_ids]             | general / comment\_update | Update the comment.                  |
|  03.  | /comment/{id}                | DELETE | session / token   | id                                                  | general / comment\_delete | Delete the comment.                  |
|  04.  | /comment/{id}                |   GET  | session / [token] | id                                                  |                           | find the comment.                    |
|  05.  | /comment/text/{id}/{page}    |   GET  | session / [token] | id / [page]                                         |                           | find the comments by the text id.    |
|  06.  | /comment/file/{id}/{page}    |   GET  | session / [token] | id / [page]                                         |                           | find the comments by the file id.    |
|  07.  | /comment/comment/{id}/{page} |   GET  | session / [token] | id / [page]                                         |                           | find the comments by the comment id. |


#### \# COUNTRY

|  No.  | Route                    | Method | Header Params | Body Params | Permission | Description                                 |
|:-----:|--------------------------|:------:|---------------|-------------|------------|---------------------------------------------|
|  01.  | /country/{lang}          |   GET  | session       | lang        |            | Find the country name list by lang.         |
|  02.  | /country/callings/{lang} |   GET  | session       | lang        |            | Find the country calling code list by lang. |


#### \# FILE

|  No.  | Route                                   | Method | Header Params     | Body Params                                                                                     | Permission             | Description                                  |
|:-----:|-----------------------------------------|:------:|-------------------|-------------------------------------------------------------------------------------------------|------------------------|----------------------------------------------|
|  01.  | /file                                   |  POST  | session / token   | [grand\_type] / grand\_id / [parent\_type] / [parent\_id] / [privacy\_status] / [status] / file | general / file\_create | Create the file.                             |
|  02.  | /file/{id}                              |   PUT  | session / token   | id / [privacy\_status] / [status] / [title] / [description] / [is\_cover]                       | general / file\_update | Update the file.                             |
|  03.  | /file/{id}                              | DELETE | session / token   | id                                                                                              | general / file\_delete | Delete the file.                             |
|  04.  | /file/{id}                              |   GET  | session / [token] | id                                                                                              |                        | find the file.                               |
|  05.  | /file/filename/{filename}               |   GET  | session / [token] | filename                                                                                        |                        | find the file by filename.                   |
|  06.  | /file/{id}/view/count                   |   PUT  | session / [token] | id                                                                                              |                        | count the view number with file.             |
|  07.  | /file/group/statistic/image/view/{page} |   GET  | session / token   | page                                                                                            | group / report\_read   | find the image file view statistic in group. |
|  08.  | /file/group/statistic/video/view/{page} |   GET  | session / token   | page                                                                                            | group / report\_read   | find the video file view statistic in group. |


#### \# GROUPS

|  No.  | Route                         | Method | Header Params   | Body Params                                                                               | Permission             | Description                          |
|:-----:|-------------------------------|:------:|-----------------|-------------------------------------------------------------------------------------------|------------------------|--------------------------------------|
|  01.  | /groups                       |  POST  | session / token | type / title / keywords / [description]                                                   |                        | Create the group.                    |
|  02.  | /groups/{id}                  |   PUT  | session / token | type / title / keywords / [description]                                                   | group / group\_update  | Update the group.                    |
|  03.  | /groups/{id}                  | DELETE | session / token | id                                                                                        | groups / group\_delete | Delete the group.                    |
|  04.  | /groups/{id}                  |   GET  | session / token | id                                                                                        |                        | find the group.                      |
|  05.  | /groups                       |   GET  | session / token |                                                                                           |                        | find this User group list.           |
|  06.  | /groups/myOwnList             |   GET  | session / token |                                                                                           |                        | find this token user group OwnList.  |
|  07.  | /groups/myAddList             |   GET  | session / token |                                                                                           |                        | find this token user  group AddList. |
|  08.  | /groups/{user_id}/getByUserId |   GET  | session / token | user\_id                                                                                  |                        | find this user\_id group List.       |
|  09.  | /groups/{id}/cover            |  POST  | session / token | id / cover / \_method                                                                     | group / group\_update  | upload cover image                   |
|  10.  | /groups/{id}/avatar           |  POST  | session / token | id / avatar / \_method                                                                    | group / group\_update  | upload avatar image                  |
|  11.  | /groups/{id}/setOwnerWindower |   PUT  | session / token | id / windower                                                                             |                        | set Owner Windower                   |
|  12.  | /groups/board                 |  POST  | session / token | [parent\_type] / parent\_id / [privacy\_status] / [status] / content / [file\_ids]        | general / text\_create | Publish the text and files.          |
|  13.  | /groups/board/{id}            |   PUT  | session / token | id / [parent\_type] / parent\_id / [privacy\_status] / [status] / [content] / [file\_ids] | general / text\_update | Update the text and files.           |
|  14.  | /groups/board/{id}            | DELETE | session / token | id                                                                                        | general / text\_delete | Delete the text and files.           |


#### \# groupUsers

|  No.  | Route                                              | Method | Header Params   | Body Params                                                                  | Permission                         | Description                                     |
|:-----:|----------------------------------------------------|:------:|-----------------|------------------------------------------------------------------------------|------------------------------------|-------------------------------------------------|
|  01.  | /groupUsers                                        |  POST  | session / token | groupId / account                                                            | group / member\_create             | Invite user in group.                           |
|  02.  | /groupUsers/join                                   |  POST  | session / token | groupId                                                                      |                                    | join this group.                                |
|  03.  | /groupUsers/{groupId}/role/{id}                    |   PUT  | session / token | groupId / id / role                                                          | group / member\_update\_permission | Change role.                                    |
|  04.  | /groupUsers/{id}/list                              |   GET  | session / token | id                                                                           |                                    | find the group all user                         |
|  05.  | /groupUsers/{id}                                   |   GET  | session / token | id                                                                           |                                    | find the group user.                            |
|  06.  | /groupUsers/{groupId}/delete/{id}                  | DELETE | session / token | groupId / id                                                                 | groups / member\_delete            | Delete the group user.                          |
|  07.  | /groupUsers/{groupId}/selfDelete/{id}              | DELETE | session / token | groupId / id                                                                 |                                    | User quit group                                 |
|  08.  | /groupUsers/{groupId}/allow/{id}                   |   PUT  | session / token | groupId / id / status                                                        | group / member\_create             | allow this user in group                        |
|  09.  | /groupUsers/{groupId}/selfAgree/{id}               |   PUT  | session / token | groupId / id / status                                                        |                                    | selfAgree this user in group                    |
|  10.  | /groupUsers/{groupId}/changeOwner/{id}             |   PUT  | session / token | groupId / id / role                                                          | group / group\_update\_owner       | no use this api. change this group owner        |
|  11.  | /groupUsers/{groupId}/meetingEnable/{id}           |   PUT  | session / token | groupId / id / meeting_enable                                                | group / member\_update\_meeting    | no use this api. change group user can meeting. |
|  12.  | /groupUsers/{groupId}/liveEnable/{id}              |   PUT  | session / token | groupId / id / live_enable                                                   | group / member\_update\_live       | no use this api. change group user can live.    |
|  13.  | /groupUsers/{groupId}/timeSetting                  |   PUT  | session / token | groupId  / start\_date / end\_date / mon / tue / web / thu / fri / sat / sun |                                    | set can meeting Time.                           |
|  14.  | /groupUsers/{groupId}/getTimeSetting               |   GET  | session / token | groupId                                                                      |                                    | get can meeting time.                           |
|  15.  | /groupUsers/{groupId}/available/{userId}/day/{day} |   GET  | session / token | groupId / userId / day                                                       |                                    | get this userId can meeting time.               |

#### \# INTEREST

|  No.  | Route            | Method | Header Params | Body Params | Permission | Description                     |
|:-----:|------------------|:------:|---------------|-------------|------------|---------------------------------|
|  01.  | /interest/{lang} |   GET  | session       | lang        |            | Find the interest list by lang. |


#### \# LANGUAGE

|  No.  | Route            | Method | Header Params | Body Params | Permission | Description                          |
|:-----:|------------------|:------:|---------------|-------------|------------|--------------------------------------|
|  01.  | /language/{lang} |   GET  | session       | lang        |            | Find the language name list by lang. |
   

#### \# LOCALES

|  No.  | Route                         | Method | Header Params     | Body Params                                           | Permission              | Description                              |
|:-----:|-------------------------------|:------:|-------------------|-------------------------------------------------------|-------------------------|------------------------------------------|
|  01.  | /locales/langs                |   GET  | session / [token] |                                                       |                         | Find the language list.                  |
|  02.  | /locales/lang/{lang}          |   GET  | session / [token] | lang                                                  |                         | Find the locales by language.            |
|  03.  | /locales/code/{code}          |   GET  | session / [token] | code                                                  |                         | Find the locales by code.                |
|  04.  | /locales/{lang}/{code}        |   GET  | session / [token] | lang / code                                           |                         | Find the locales by language and code.   |
|  05.  | /locales                      |  POST  | session / token   | code / note / lang\_\{lang\} / content\_\{lang\}      | admin / locales\_create | Create the locales and contents.         |
|  06.  | /locales/{id}                 |   PUT  | session / token   | id / code / note / lang\_\{lang\} / content\_\{lang\} | admin / locales\_update | Update the locales and contents.         |
|  07.  | /locales/{id}                 | DELETE | session / token   | id                                                    | admin / locales\_delete | Delete the locales and contents.         |
|  08.  | /locales/lang/{lang}/makefile |   PUT  | session / token   | lang                                                  | admin / locales\_update | Make the locales JSON files by language. |
|  09.  | /locales/all                  |   GET  | session / [token] |                                                       |                         | Find the all locales.                    |

#### \# NOTICES

|  No.  | Route                               | Method | Header Params   | Body Params         | Permission | Description                                   |
|:-----:|-------------------------------------|:------:|-----------------|---------------------|------------|-----------------------------------------------|
|  01.  | /notices/{id}/readed                |   PUT  | session / token |                     |            | readed this notice                            |
|  02.  | /notices/{id}                       | DELETE | session / token | id                  |            | Delete the notice.                            |
|  03.  | /notices/{id}                       |   GET  | session / token | id                  |            | find the notice.                              |
|  04.  | /notices/{parentType}/{status}/list |   GET  | session / token | parentType / status |            | find this parentType and status  notice list. |

#### \# MEETINGS

|  No.  | Route                         | Method | Header Params   | Body Params                                                             | Permission | Description                   |
|:-----:|-------------------------------|:------:|-----------------|-------------------------------------------------------------------------|------------|-------------------------------|
|  01.  | /meetings                     |  POST  | session / token | group\_id / user\_id / meeting\_date / meeting\_time / meeting\_summary |            | Create the meeting.           |
|  02.  | /meetings/zoomCreate          |  POST  | session / token | meeting\_id / user\_id                                                  |            | Create the zoom.              |
|  03.  | /meetings/tmpZoomCreate       |  POST  | session / token | group\_id / user\_id                                                    |            | Create the tmpzoom.           |
|  04.  | /meetings/{id}/agree          |   PUT  | session / token | id                                                                      |            | agree this meeting            |
|  05.  | /meetings/{id}                | DELETE | session / token | id / cancel\_note meeting\_date / meeting\_time / meeting\_summary      |            | Delete the meeting.           |
|  06.  | /meetings/{id}                |   GET  | session / token | id                                                                      |            | find the meeting.             |
|  07.  | /meetings/{status}/list       |   GET  | session / token | status                                                                  |            | find this status meeting list |
|  08.  | /meetings/{id}/cancelDelete   | DELETE | session / token | id / cancel\_note meeting\_date / meeting\_time meeting\_summary        |            | Booker delete the meeting.    |
|  09.  | /meetings/{status}/bookerList |   GET  | session / token | status                                                                  |            | find this booker meeting list |

#### \# OTHERS

|  No.  | Route     | Method | Header Params     | Body Params                                                                                                                            | Permission | Description |
|:-----:|-----------|:------:|-------------------|----------------------------------------------------------------------------------------------------------------------------------------|------------|-------------|
|  01.  | /register |  POST  | session           | account / password / password\_repeat / first\_name / last\_name / mobile\_country\_code / mobile\_phone / qrcode\_id / introducer\_id |            | Register    |
|  02.  | /login    |  POST  | session           | [account] / [mobile\_country\_code] [mobile\_phone] / password                                                                         |            | Login       |
|  03.  | /logout   |  POST  | session / [token] |                                                                                                                                        |            | Logout      |


#### \# QRCODES

|  No.  | Route                        | Method | Header Params   | Body Params                      | Permission             | Description                 |
|:-----:|------------------------------|:------:|-----------------|----------------------------------|------------------------|-----------------------------|
|  01.  | /qrcodes                     |  POST  | session / token | type / [parentType] / [parentId] |                        | Create the qrcode.          |
|  02.  | /qrcodes/{id}/updatenote     |   PUT  | session / token | id / note                        |                        | update this qrcode note.    |
|  03.  | /qrcodes/{id}                | DELETE | session / token | id                               |                        | Delete the qrcode.          |
|  04.  | /qrcodes/{id}                |   GET  | session / token | id                               |                        | find the qrcode.            |
|  05.  | /qrcodes                     |   GET  | session / token |                                  |                        | find this user qrcode list  |
|  06.  | /qrcodes/{groupId}/groupList |   GET  | session / token | groupId                          | group / member\_create | find this group qrcode list |
|  07.  | /qrcodes/{id}/check          |   GET  | session / token | id                               |                        | check qrcode no expire.     |


#### \# ROLE_PERMISSION

|  No.  | Route               | Method | Header Params     | Body Params                                                                                                                    | Permission           | Description                      |
|:-----:|---------------------|:------:|-------------------|--------------------------------------------------------------------------------------------------------------------------------|----------------------|----------------------------------|
|  01.  | /role/{id}          |   GET  | session / [token] | id                                                                                                                             |                      | Find the role by id.             |
|  02.  | /role/type/{type}   |   GET  | session / [token] | type                                                                                                                           |                      | Find the roles by type.          |
|  03.  | /role/name/{name}   |   GET  | session / [token] | name                                                                                                                           |                      | Find the roles by name.          |
|  04.  | /role/{type}/{name} |   GET  | session / [token] | type / name                                                                                                                    |                      | Find the role by type and name.  |
|  05.  | /role               |  POST  | session / token   | role\_type / role\_name / role\_description / permissions [permission\_type / permission\_name / permission\_description]      | admin / role\_create | Create the role and permissions. |
|  06.  | /role/{id}          |   PUT  | session / token   | id / role\_type / role\_name / role\_description / permissions [permission\_type / permission\_name / permission\_description] | admin / role\_update | Update the role and permissions. |
|  07.  | /role/{id}          | DELETE | session / token   | id                                                                                                                             | admin / role\_delete | Delete the role and permissions. |


#### \# SEARCH

|  No.  | Route          | Method | Header Params     | Body Params | Permission | Description                                             |
|:-----:|----------------|:------:|-------------------|-------------|------------|---------------------------------------------------------|
|  01.  | /search/{term} |   GET  | session / [token] | term        |            | Find the group title and user first\_name / last\_name. |


#### \# SESSION

|  No.  | Route           | Method | Header Params | Body Params                                                                                         | Permission | Description       |
|:-----:|-----------------|:------:|---------------|-----------------------------------------------------------------------------------------------------|------------|-------------------|
|  01.  | /session/init   |  POST  |               | device\_code / [device\_os] / [device\_type] / [device\_lang] / [device\_token] / [device\_channel] |            | Init the session. |
|  02.  | /session/{code} |   GET  |               | code                                                                                                |            | Find the session. |


#### \# USER

|  No.  | Route                     | Method | Header Params     | Body Params                                                                                                                   | Permission               | Description                             |
|:-----:|---------------------------|:------:|-------------------|-------------------------------------------------------------------------------------------------------------------------------|--------------------------|-----------------------------------------|
|  01.  | /user/{id}                |   GET  | session / [token] | id                                                                                                                            |                          | Find the user.                          |
|  02.  | /user/token/{token}       |   GET  | session           | token                                                                                                                         |                          | Find the user by token.                 |
|  03.  | /user/{id}/password       |   PUT  | session / token   | id / password / password\_repeat                                                                                              | general / member\_update | Change the Password.                    |
|  04.  | /user/{id}/avatar         |   PUT  | session / token   | id / avatar / \_method                                                                                                        | general / member\_update | Upload the avatar file.                 |
|  05.  | /user/password/notify     |  POST  | session           | account                                                                                                                       |                          | Send the notify for reset the password. |
|  06.  | /user/{id}/password/reset |   PUT  | session           | id / account / password / password\_repeat / check\_code                                                                      |                          | Reset the password.                     |
|  07.  | /user/{id}/email/notify   |   GET  | session           | id                                                                                                                            |                          | Send the notify for verify the email.   |
|  08.  | /user/{id}/email/verify   |   PUT  | session           | id / check\_code                                                                                                              |                          | Verify the email.                       |
|  09.  | /user/{id}/mobile/notify  |   GET  | session           | id                                                                                                                            |                          | Send the notify for verify the mobile.  |
|  10.  | /user/{id}/mobile/verify  |   PUT  | session           | id / check\_code                                                                                                              |                          | Verify the mobile.                      |
|  11.  | /user/{id}/qrcode/verify  |   PUT  | session           | id / user_id                                                                                                                  |                          | Verify the qrcode by recruiter.         |
|  12.  | /user/{id}/profile        |   PUT  | session / token   | id / first\_name / last\_name / nickname / gender / birth / city / intro / website / languages / interests / countries / pets | general / member\_update | Update the user profile.                |
|  13.  | /user/{id}/role           |   GET  | session / [token] | id                                                                                                                            |                          | Find the user roles and permissions.    |
|  14.  | /user/{id}/role           |   PUT  | session / token   | id / role\_id                                                                                                                 | admin / user\_update     | Update the user's role.                 |


#### \# USER_EDUCATION

|  No.  | Route                             | Method | Header Params     | Body Params                                                                                                                                     | Permission               | Description                                  |
|:-----:|-----------------------------------|:------:|-------------------|-------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------|----------------------------------------------|
|  01.  | /user/education                   |  POST  | session / token   | school / [description] / [type] / [start\_date] / [end\_date] / [graduated] / [privacy\_status] / [status] / [degree] / [concentrations]        | general / member\_update | Create User School.                          |
|  02.  | /user/education/{id}              |   PUT  | session / token   | id / [school] / [description] / [type] / [start\_date] / [end\_date] / [graduated] / [privacy\_status] / [status] / [degree] / [concentrations] | general / member\_update | Update User School.                          |
|  03.  | /user/education/{id}              | DELETE | session / token   | id                                                                                                                                              | general / member\_update | Delete User School.                          |
|  04.  | /user/education/{id}              |   GET  | session / [token] | id                                                                                                                                              |                          | Find User School by user education id.       |
|  05.  | /user/{id}/educations/{page}      |   GET  | session / [token] | id / [page]                                                                                                                                     |                          | Find User School by user id.                 |
|  06.  | /user/{id}/educations/type/{type} |   GET  | session / [token] | id / type                                                                                                                                       |                          | Find User School by user id and school type. |

#### \# USER_FRIENDS

|  No.  | Route                                      | Method | Header Params   | Body Params     | Permission | Description                                  |
|:-----:|--------------------------------------------|:------:|-----------------|-----------------|------------|----------------------------------------------|
|  01.  | /userFriends                               |  POST  | session / token | friend\_id      |            | Create userFriends the status is init.       |
|  02.  | /userFriends/{id}/agreeFriends             |   PUT  | session / token | id / status     |            | update this userFriends status.              |
|  03.  | /userFriends/{id}                          | DELETE | session / token | id              |            | Delete the userFriends.                      |
|  04.  | /userFriends/{id}                          |   GET  | session / token | id              |            | find the userFriends.                        |
|  05.  | /userFriends/{status}/list                 |   GET  | session / token | status          |            | find this user status userFriends list       |
|  06.  | /userFriends/{status}/getByUserId/{userId} |   GET  | session / token | status / userId |            | find this UserId and status userFriends list |


#### \# USER_WORK

|  No.  | Route                   | Method | Header Params     | Body Params                                                                                                                           | Permission               | Description                     |
|:-----:|-------------------------|:------:|-------------------|---------------------------------------------------------------------------------------------------------------------------------------|--------------------------|---------------------------------|
|  01.  | /user/work              |  POST  | session / token   | company / position / [city] / [description] / [start\_date] / [end\_date] / [working\_status] / [privacy\_status] / [status]          | general / member\_update | Create User Work.               |
|  02.  | /user/work/{id}         |   PUT  | session / token   | id / [company] / [position] / [city] / [description] / [start\_date] / [end\_date] / [working\_status] / [privacy\_status] / [status] | general / member\_update | Update User Work.               |
|  03.  | /user/work/{id}         | DELETE | session / token   | id                                                                                                                                    | general / member\_update | Delete User Work.               |
|  04.  | /user/work/{id}         |   GET  | session / [token] | id                                                                                                                                    |                          | Find User Work by user work id. |
|  05.  | /user/{id}/works/{page} |   GET  | session / [token] | id / [page]                                                                                                                           |                          | Find User Work by user id.      |

