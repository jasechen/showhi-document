### 環境版本  

+ Ubuntu 16.04.3 LTS  
+ Apache 2.4.18  
+ PHP 7.1.11  
+ MySQL 5.7.19  


### Apache / PHP 模組  

+ WEB API Server  
	`http://api.w.test.beta.showhi.co/phpinfo.php` [![](https://png.icons8.com/external-link/win/16/2980b9)](http://api.w.test.beta.showhi.co/phpinfo.php)   

+ Mobile API Server  
	`http://api.m.test.beta.showhi.co/phpinfo.php` [![](https://png.icons8.com/external-link/win/16/2980b9)](http://api.m.test.beta.showhi.co/phpinfo.php)  


### 伺服器資訊

+ WEB API Server  
	Host : api.w.test.beta.showhi.co  
	Key : showhi-cloud-beta.pem  
	Gitlab : `https://gitlab.com/showhi/api-web-beta` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://gitlab.com/showhi/api-web-beta) 

+ Mobile API Server  
  Host : api.m.test.beta.showhi.co  
  Key : showhi-cloud-beta.pem  
  Gitlab : `https://gitlab.com/showhi/api-mobile-beta` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://gitlab.com/showhi/api-mobile-beta) 

+ WEB Server  
  Host : test.beta.showhi.co  
  Key : showhi-cloud-beta.pem  
  Gitlab : `https://gitlab.com/showhi/web-beta` [![](https://png.icons8.com/external-link/win/16/2980b9)](https://gitlab.com/showhi/web-beta) 

+ MySQL  
  Host : beta-test-master-01.cv71ff05zjr4.ap-northeast-1.rds.amazonaws.com  
  PHPMyAdmin : `http://api.w.test.beta.showhi.co/mysql` [![](https://png.icons8.com/external-link/win/16/2980b9)](http://api.w.test.beta.showhi.co/mysql)   

