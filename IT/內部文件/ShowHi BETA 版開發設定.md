### Using Eloquent  [![](https://png.icons8.com/external-link/win/16/2980b9)](https://d.laravel-china.org/docs/5.5/eloquent) 及 Facades [![](https://png.icons8.com/external-link/win/16/2980b9)](https://laravel-china.org/topics/954/lumen-first-experience-two#Facades)  

編輯 `bootstrap/app.php`  

	# uncomment
	#
	// $app->withFacades();		=>		$app->withFacades();
	// $app->withEloquent();	=>		$app->withEloquent();


### Read & Write Connections with Multi Databases  

**`How to Create Multiple Database Connections in Lumen`** [![](https://png.icons8.com/external-link/win/16/2980b9)](http://webrewrite.com/how-to-create-multiple-database-connections-in-lumen/)  

1. 編輯 `.env`  

		# config
		# 
		DB_HOST=localhost
		DB_PORT=3306
		DB_DATABASE=example1
		DB_USERNAME=root
		DB_PASSWORD=
		
		DB_HOST_READ=localhost
		DB_PORT_READ=3306
		DB_DATABASE_READ=example2
		DB_USERNAME_READ=readroot
		DB_PASSWORD_READ=		

2. 編輯 `config/database.php`

		# config
		#
		'connections' => [
			// default(Write) DB connect 
			'mysql' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST', 'localhost'),
				'port'      => env('DB_PORT', 3306),
				'database'  => env('DB_DATABASE', 'showhi'),
				'username'  => env('DB_USERNAME', 'showhi'),
				'password'  => env('DB_PASSWORD', ''),
				'charset'   => env('DB_CHARSET', 'utf8'),
				'collation' => env('DB_COLLATION', 'utf8_general_ci'),
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
			// Second(Read) DB connect
			'read-replica' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST_READ', 'localhost'),
				'port'      => env('DB_PORT_READ', 3306),
				'database'  => env('DB_DATABASE_READ', 'showhi'),
				'username'  => env('DB_USERNAME_READ', 'showhi'),
				'password'  => env('DB_PASSWORD_READ', ''),
				'charset'   => env('DB_CHARSET', 'utf8'),
				'collation' => env('DB_COLLATION', 'utf8_general_ci'),
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
		]
	
3. 使用

	+ Raw SQL Queries

			# default(write) connection
			#
			app('db')->connection()->select('SELECT * FROM employees');         

			# read-replica connection
			#
			app('db')->connection('read-replica')->select('SELECT * FROM employees');

	+ Query Builder  

			# default(write) connection
			#
			DB::table($this->table)
			->select(DB::raw($this->_employeeAttributes))
			->where('employee_id',$employeeId)
			->first()

			# read-replica connection
			#
			DB::connection('read-replica')
			->table($this->table)
			->select(DB::raw($this->_employeeAttributes))
			->where("employee_id", $employeeId)
			->get();


### 版本控制策略
團隊選擇使用 Git 作為版本控制工具，有關 Git 的使用及相關訊息，請看 git book [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://git-scm.com/book/zh-tw/v2)、ihower 的 Git 教室 [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://ihower.tw/git/index.html)。

**以下為 Git Flow 介紹:**  

+ Git flow 開發流程 [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://ihower.tw/blog/archives/5140)
+ 什麼是Git flow ? 如何在SourceTree使用Git flow管理開發 [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://www.takobear.tw/2014/02/15/bear-git-flow-sourcetreegit-flow/)
+ git-flow 的工作流程 [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://www.git-tower.com/learn/git/ebook/cn/command-line/advanced-topics/git-flow)
+ 成功的 Git 分支模式 [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://blog.buginception.com/blog/2012/10/13/recap-git-branching-model/)  

**Github 使用 git 的策略:**  

+ Git flow 與團隊合作 [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://www.slideshare.net/appleboy/git-flow-61442567)
+ 在 GitHub 當中使用的 work flow [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://blog.krdai.info/post/17485259496/github-flow)

	
### 開發工具
為了團隊開發順利，雖然不限制成員使用的 IDE 或編輯器，但有些規範請大家遵守。
若使用 Sublime Text 可參考「SublimeText3 設定及套件」[![](https://png.icons8.com/external-link/win/16/2980b9)] (https://goo.gl/r8Vxj9)

+ PHP 開發遵守 PSR 規範 [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://oomusou.io/php/php-psr2/)  
(Laravel 使用 PSR-4 規範，工作坊 PHP 語法基礎與物件導向 Page48[![](https://png.icons8.com/external-link/win/16/2980b9)] (https://goo.gl/guEtaQ))
+ 檔案存為無 BOM 的 UTF-8 格式
+ 消除 Windows \^M 斷行
+ 每行結尾不要留有多於空格


### 開發建議規範

**PHP**  

+ PSR-2 PHP Coding Style [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://oomusou.io/php/php-psr2/) 
+ Clean Code PHP [英文版 ![](https://png.icons8.com/external-link/win/16/2980b9)] (https://github.com/jupeter/clean-code-php)、[簡中版 ![](https://png.icons8.com/external-link/win/16/2980b9)] (https://github.com/php-cpm/clean-code-php) 
+ Clean Code 實戰之 PHP 良好實踐 [![](https://png.icons8.com/external-link/win/16/2980b9)] (https://kylinyu.win/php_best_practice) 

**MySQL**

+ MySQL開發規範與使用技巧總結 [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://www.itread01.com/articles/1478281233.html) 
+ sql 查询慢的48个原因分析 [![](https://png.icons8.com/external-link/win/16/2980b9)] (http://database.ctocio.com.cn/222/9068222.shtml) 

	
### 開發過程中，設定檔依環境的轉換方式

1. 在 `.gitignore` 中設定 `.env`，在 git push 時就不會把 Dev 環境中的 `.env` 上到對應的 branch 中，也就不會影響到 git pull 的 Test / Production 環境了。

2. 在 config 裡有關邏輯的各種設定檔，可用 [這裡 ![](https://png.icons8.com/external-link/win/16/2980b9)] (https://laravel.com/docs/5.5/configuration#environment-configuration) 提到的方式。利用 `.env` 裡的 APP_ENV 現行環境的設定，來使用相關環境的設定值。


### RESTful 錯誤時 HTTP Status Code

+ 400 : 輸入的變數為空值

		Example:
		===
		$name = $request->input('name');
		if (empty($name)) {
			$code = 400;
			$comment = 'name is empty';
			$this->failResponse($comment, $code);
		} // END if
	
+ 401 : 未授權
+ 403 : 有授權，但禁止
	
		Example:
		===
		if ($sessionDeadline < $now->timestamp) {
		    $code = 403;
		    $comment = 'session expire time';
		    $this->failResponse($comment, $code);
		} // END if

+ 404 : 不存在

		Example 1:
		===  
		if (!in_array($deviceOS, config('device.DEVICE_OS'))) {
			$code = 404;
			$comment = 'device os error';
			$this->failResponse($comment, $code);
		} // END if


		Example 2:
		===  
		$session = $this->sessionRepository->findByCode($sessionCode);
		if ($session->isEmpty()) {
			$code = 404;
			$comment = 'session not found';
			$this->failResponse($comment, $code);
		} // END if
                
+ 422 : 新增 (insert/add/create/save) 或更新 (update/save) 時失敗  
		
		Example:
		===  
		$this->userRepository->create($userFields);
		$user = $this->userRepository->findByAccount($account);
		if ($user->isEmpty()) {
			$code = 422;
			$comment = 'create user fail';
			$this->failResponse($comment, $code);
		} // END if

