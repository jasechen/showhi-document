	<?php

	return [

		...
	
	    'connections' => [
	
			...
	
			'mysql' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST', '127.0.0.1'),
				'port'      => env('DB_PORT', '3306'),
				'database'  => env('DB_DATABASE', 'forge'),
				'username'  => env('DB_USERNAME', 'forge'),
				'password'  => env('DB_PASSWORD', ''),
				'charset'   => 'utf8',
				'collation' => 'utf8_general_ci',
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
			
			'read-main' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST_READ', '127.0.0.1'),
				'port'      => env('DB_PORT_READ', 3306),
				'database'  => env('DB_DATABASE_READ', 'forge'),
				'username'  => env('DB_USERNAME_READ', 'forge'),
				'password'  => env('DB_PASSWORD_READ', ''),
				'charset'   => env('DB_CHARSET', 'utf8'),
				'collation' => env('DB_COLLATION', 'utf8_general_ci'),
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
	
			'log' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST_LOG', '127.0.0.1'),
				'port'      => env('DB_PORT_LOG', '3306'),
				'database'  => env('DB_DATABASE_LOG', 'forge'),
				'username'  => env('DB_USERNAME_LOG', 'forge'),
				'password'  => env('DB_PASSWORD_LOG', ''),
				'charset'   => 'utf8',
				'collation' => 'utf8_general_ci',
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
			
			'read-log' => [
				'driver'    => 'mysql',
				'host'      => env('DB_HOST_LOG_READ', '127.0.0.1'),
				'port'      => env('DB_PORT_LOG_READ', 3306),
				'database'  => env('DB_DATABASE_LOG_READ', 'forge'),
				'username'  => env('DB_USERNAME_LOG_READ', 'forge'),
				'password'  => env('DB_PASSWORD_LOG_READ', ''),
				'charset'   => env('DB_CHARSET', 'utf8'),
				'collation' => env('DB_COLLATION', 'utf8_general_ci'),
				'prefix'    => env('DB_PREFIX', ''),
				'timezone'  => env('DB_TIMEZONE', '+08:00'),
				'strict'    => env('DB_STRICT_MODE', false),
			],
	
			...
	
	    ],
	
		...

];
