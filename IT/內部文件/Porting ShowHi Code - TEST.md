## Porting ShowHi Code - TEST env.

### \# Database

1. 新增資料庫 `showhi_main`

        CREATE DATABASE `showhi_main` CHARACTER SET `utf8` COLLATE `utf8_general_ci` 

2. 新增資料庫 `showhi_log`

        CREATE DATABASE `showhi_log` CHARACTER SET `utf8` COLLATE `utf8_general_ci` 


### \# Porting Code

1. 從 GitLab clone 測試分支程式 至 目標資料夾

        % git clone -b test https://gitlab.com/showhi/api-web-beta.git {TARGET_DIRECTORY}

2. cd 至 目標資料夾

        % cd {TARGET_DIRECTORY}   

3. 新增 `.env`

        % joe .env
        
        # add
        # 
        APP_ENV=staging
        APP_DEBUG=true
        APP_NAME=showhi
        APP_URL={TEST_URL}
        APP_KEY={LARAVEL_KEY}
        APP_TIMEZONE=Asia/Taipei

        DB_CONNECTION=mysql
        DB_HOST={MAIN_DATABASE_HOST}
        DB_PORT=3306
        DB_DATABASE=showhi_main
        DB_USERNAME={MAIN_DATABASE_USERNAME}
        DB_PASSWORD={MAIN_DATABASE_PASSWORD}
        DB_STRICT_MODE=true

        DB_HOST_READ={SLAVE_DATABASE_HOST}
        DB_PORT_READ=3306
        DB_DATABASE_READ=showhi_main
        DB_USERNAME_READ={SLAVE_DATABASE_USERNAME}
        DB_PASSWORD_READ={SLAVE_DATABASE_PASSWORD}
        DB_STRICT_MODE_READ=false

        DB_HOST_LOG={MAIN_DATABASE_HOST}
        DB_PORT_LOG=3306
        DB_DATABASE_LOG=showhi_log
        DB_USERNAME_LOG={MAIN_DATABASE_USERNAME}
        DB_PASSWORD_LOG={MAIN_DATABASE_PASSWORD}

        DB_HOST_LOG_READ={SLAVE_DATABASE_HOST}
        DB_PORT_LOG_READ=3306
        DB_DATABASE_LOG_READ=showhi_log
        DB_USERNAME_LOG_READ={SLAVE_DATABASE_USERNAME}
        DB_PASSWORD_LOG_READ={SLAVE_DATABASE_PASSWORD}

        CACHE_DRIVER=file
        QUEUE_DRIVER=sync

        SESSION_DRIVER=file
        SESSION_LIFETIME=86400
        SESSION_DOMAIN=showhi.co
        SESSION_SECURE_COOKIE=false

        MAIL_DRIVER=smtp
        MAIL_HOST=smtp.gmail.com
        MAIL_PORT=587
        MAIL_ENCRYPTION=tls
        MAIL_USERNAME={GMAIL_USERNAME}
        MAIL_PASSWORD={GMAIL_PASSWORD}

        SNOWFLAKE_WORKER_ID=1
        SNOWFLAKE_DATACENTER_ID=1

        TWILIO_SID={TWILIO_SID}
        TWILIO_TOKEN={TWILIO_TOKEN}
        TWILIO_FROM={TWILIO_SOURCE_PHONE_NUMBER}

        AWS_ACCESS_KEY_ID={AWS_ACCESS_KEY_ID}
        AWS_SECRET_ACCESS_KEY={SECRET_ACCESS_KEY}
        AWS_REGION={AWS_REGION}
        AWS_DEBUG=true
        AWS_BUCKET_LOCALES=showhi-test-asset
        AWS_BUCKET_GALLERY=showhi-test-gallery
        AWS_BUCKET_QRCODE=showhi-test-qrcode

4. composer 安裝所需程式

        % php composer.phar install

5. 編輯 `vendor/aloha/twilio/src/Support/Laravel/ServiceProvider.php`

        % joe vendor/aloha/twilio/src/Support/Laravel/ServiceProvider.php

        # edit
        #
        use Illuminate\Foundation\Application;        
        =>
        // use Illuminate\Foundation\Application;
        ===        

        if (version_compare(Application::VERSION, '5.0', '<')) {
            $this->provider = new L4ServiceProvider($this->app);
        } else {
            $this->provider = new L5ServiceProvider($this->app);
        }
        
        =>
        
        // if (version_compare(Application::VERSION, '5.0', '<')) {
        //    $this->provider = new L4ServiceProvider($this->app);
        // } else {
             $this->provider = new L5ServiceProvider($this->app);
        // }

6. 編輯 `vendor/kra8/laravel-snowflake/src/HasSnowflakePrimary.php`

        % joe vendor/kra8/laravel-snowflake/src/HasSnowflakePrimary.php

        # edit
        #
        $id = resolve(Snowflake::class)->next();
        => 
        $id = app(Snowflake::class)->next();
    
7. 利用 artisan 產生資料表

        % php artisan migrate

8. 利用 artisan 新增預設資料

        % php artisan db:seed
    