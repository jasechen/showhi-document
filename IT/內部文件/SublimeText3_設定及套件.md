### \# PACKAGES

**1337 Color Scheme**  
> 1337 is a color-scheme for dark Sublime Text   
>

**AngularJS**  
> + 提供許多 AngularJS 開發過程所需的自動完成 (Auto-completion) 需求
> + 常見的 AngularJS 工具函式與 ng 模組中內建的 services 都有提供 Intellisense
> + 可快速跳躍至選中的 directive, filter, ...
>

**AutoFileName**
> 可以在輸入 URL 或圖片網址時，自動提供路徑或檔名建議 (autocompletes filenames)

**Bower**
> 可以直接從 Sublime Text 直接執行 Bower 安裝命令 (Ctrl+Shift+P)
> 

**BracketHighlighter**  
> 可自動顯示 HTML 標籤或 JavaScript 的各種對應區塊
>

**ConvertToUTF8**
> 會自動偵測文件的編碼字集(Charset)，轉成 UTF-8。   
>
> 由於正體字與簡體字的字集有許多重疊，所以再自動判斷字集方便，難免會有所失誤，這個 ConvertToUTF8 套件由於是對岸大牛所設計的，預設判斷字集的優先順序是先判斷是否為簡體字 (GBK) 再判斷是否為繁體字 (BIG5)，如果你想要改變這個預設偵測文件字集的順位，可以參考以下設定步驟調整。   
>
> + 將 %APPDATA%\Sublime Text 3\Packages\ConvertToUTF8\ConvertToUTF8.sublime-settings" 預設設定檔，複製到 %APPDATA%\Sublime Text 3\Packages\User\ 目錄下。
> + 然後直接透過 Sublime Text 3 編輯這個位於 User 目錄下的 ConvertToUTF8.sublime-settings 檔案。將 encoding_list 前兩個字集定義對調即可，即讓 BIG5 擺在首位。
> 

**DocBlockr**
> 自動產生區塊是註解的工具，可用在 JS, CSS, PHP, CoffeeScript, Actionscript, C & C++ 等等
>

**Dracula Color Scheme**
>
>

**Emmet**
> + Emmet Documentation [![](https://png.icons8.com/external-link/win/16/2980b9)](http://docs.emmet.io/)  
> + Emmet for Sublime Text [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/sergeche/emmet-sublime) (GitHub) (有許多 Sublime 的用法與說明)
> + 使用者定義的設定檔參考內容: Emmet.sublime-settings [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/sergeche/emmet-sublime/blob/master/Emmet.sublime-settings)  
>


**Function Name Display**
> 顯示目前函式所屬的類別名稱
>
> 到 Sublime Text > Preferences > Package Settings > Function Name Display > Settings – User 修改相關的設定
>

**Git**
> 安裝完成後，只要先��下 Ctrl+Shift+P 再按照你原本輸入 Git 指令的方式選擇想執行的動作即可 
>

**Grunt**
> 可以直接從 Sublime Text 直接執行 Grunt 工作 (Ctrl+Shift+P)
>

**HTML-CSS-JS Prettify**
>
>

**LiveStyle**
> 這套必須跟 Google Chrome 做搭配，使用前必須先安裝 Chrome 下的 Emmet LiveStyle 擴充套件 [![](https://png.icons8.com/external-link/win/16/2980b9)](https://chrome.google.com/webstore/detail/diebikgmpmeppiilkaijjbdgciafajmg)  
>
> + 先把網頁打開 ( 本機網頁或線上網頁都可以 )
> + 開啟 Chrome 開發者工具，切換到 LiveStyle 頁籤，勾選 Enable LiveStyle for current page 啟用
> + 若你開啟的網頁是線上網頁，則必須明確指定這頁的 CSS 對應 (File mapping) 到本機的哪個檔案
> + 在 Sublime Text 3 開啟該網頁的 CSS 檔案 ( 必須是從外部載入的 CSS 檔案才行 )
> + 接下來，你的 Sublime Text 3 與 Chrome 開發者工具已經建立好即時連線，CSS 可雙向編輯！
> 

**MarkdownEditing**
> 不僅可以高亮顯示 Markdown 語法還支持很多程式語言的語法高亮顯示。
>

**OmniMarkupPreviewer**
> 用來預覽 Markdown 編輯的效果，同樣支持渲染代碼高亮的樣式。
>

**Package Control**
> 安裝管理 Sublime Text 套件的管理套件
>

**Phpcs**
>
>

**SideBarEnhancements**
> 提供許多側邊攔 (SideBar) 的右鍵選單功能，非常實用！ ( 按下 Ctrl+K+B 可顯示/隱藏側邊攔 )
>

**SublimeLinter / SublimeLinter-csslint / SublimeLinter-jscs / SublimeLinter-jshint /  SublimeLinter-json / SublimeLinter-php / SublimeLinter-phpcs**
> 程式碼檢測工具，利用 node.js 進行套件的安裝與運行
>

**TrailingSpaces**  
> 可自動顯示每一行後面出現的多餘空白字元，並可透過 TrailingSpaces 命令刪除 (Ctrl+Shift+P)
> 
      

### \# PREFERENCE SETTINGS
設定 `Sublime Text > Preferences > Settings`  

	{
		"color_scheme": "Packages/User/SublimeLinter/Monokai Bright (SL).tmTheme",
		"folder_exclude_patterns":
		[
			".git"
		],
		"font_face": "Ubuntu Mono",
		"font_size": 19,
		"highlight_line": true,
		"highlight_modified_tabs": true,
		"ignored_packages":
		[
			"Vintage"
		],
		"translate_tabs_to_spaces": true,
		"trim_trailing_white_space_on_save": true,
		"wide_caret": true,
		"word_separators": "./\\()\"'-:,.;<>~!@#%^&*|+=[]{}`~?",
		"word_wrap": false,
		"default_line_ending": "unix",
		"show_line_endings": true,
	}

      
### \# RESOURCES
+ Sublime Text 3 新手上路：必要的安裝、設定與基本使用教學 [![](https://png.icons8.com/external-link/win/16/2980b9)](https://blog.miniasp.com/post/2014/01/06/Useful-tool-Sublime-Text-3-Quick-Start.aspx)
+ [Sublime Text] 用 Function Name Display 顯示函式所屬的類別 class [![](https://png.icons8.com/external-link/win/16/2980b9)](https://ephrain.net/sublime-text-%E7%94%A8-function-name-display-%E9%A1%AF%E7%A4%BA%E5%87%BD%E5%BC%8F%E6%89%80%E5%B1%AC%E7%9A%84%E9%A1%9E%E5%88%A5-class/)
+ Sublime text 3 使用 SublimeLinter 檢查 Javascript, HTML, CSS, php, js等語法錯誤 [![](https://png.icons8.com/external-link/win/16/2980b9)](https://sidesigner.net/wp/78/sublime-javascript-html-css-js-php/)
+ Sublime Text 3 使用详解 [![](https://png.icons8.com/external-link/win/16/2980b9)](http://www.cnblogs.com/loveyunk/p/5735487.html)
+ 刪除不必要的空白 [![](https://png.icons8.com/external-link/win/16/2980b9)](https://www.camdemy.com/media/7333)
+ 消除 Windows ^M 斷行符號 [![](https://png.icons8.com/external-link/win/16/2980b9)](https://forum.sublimetext.com/t/sublime-adds-control-m-in-lines/4976)
