### \# DESCRIPTION
The PHP Coding Standards Fixer (PHP CS Fixer) tool fixes your code to follow standards. [![](https://png.icons8.com/external-link/win/16/2980b9)](https://github.com/FriendsOfPHP/PHP-CS-Fixer) 


### \# INSTALLATION

1. Download `php-cs-fixer`  

		% wget http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -O php-cs-fixer

	OR
	
		% wget https://github.com/FriendsOfPHP/PHP-CS-Fixer/releases/download/v2.8.1/php-cs-fixer.phar -O php-cs-fixer
		
	OR
	
		% curl -L http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -o php-cs-fixer
		
2. Change mode and Move to `/usr/local/bin/`

		% sudo chmod a+x php-cs-fixer
		% sudo mv php-cs-fixer /usr/local/bin/php-cs-fixer


### \# USAGE   

	% php-cs-fixer fix /path/to/project --rules=@PSR2 --verbose
