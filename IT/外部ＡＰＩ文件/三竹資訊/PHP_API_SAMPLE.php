﻿<?php
class SocketHttpRequest
{
    var $sHostAdd;
    var $sUri;
    var $iPort;  
    var $sRequestHeader; 
    var $sResponse;
   
    function HttpRequest($sUrl)
    {
        $sPatternUrlPart = '/http:\/\/([a-z-\.0-9]+)(:(\d+)){0,1}(.*)/i';
        $arMatchUrlPart = array();
        preg_match($sPatternUrlPart, $sUrl, $arMatchUrlPart);
       
        $this->sHostAdd = gethostbyname($arMatchUrlPart[1]);
        if (empty($arMatchUrlPart[4]))
        {
            $this->sUri = '/';
        }
        else
        {
            $this->sUri = $arMatchUrlPart[4];
        }
        if (empty($arMatchUrlPart[3]))
        {
            $this->iPort = 9600;
        }
        else
        {
            $this->iPort = $arMatchUrlPart[3];
        }
       
        $this->addRequestHeader('Host: '.$arMatchUrlPart[1]);
        $this->addRequestHeader('Connection: Close');
 
    }
   
    function addRequestHeader($sHeader)
    {
        $this->sRequestHeader .= trim($sHeader)."\r\n";
    }
   
    function sendRequest($sMethod = 'GET', $sPostData = '')
    {
        $sRequest = $sMethod." ".$this->sUri." HTTP/1.1\r\n";
        $sRequest .= $this->sRequestHeader;
        if ($sMethod == 'POST')
        {
            $sRequest .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $sRequest .= "Content-Length: ".strlen($sPostData)."\r\n";
            $sRequest .= "\r\n";
            $sRequest .= $sPostData."\r\n";
        }
        $sRequest .= "\r\n";
       
        $sockHttp = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if (!$sockHttp)
        {
            die('socket_create() failed!');
        }
       
        $resSockHttp = socket_connect($sockHttp, $this->sHostAdd, $this->iPort);
        if (!$resSockHttp)
        {
            die('socket_connect() failed!');
        }
       
        socket_write($sockHttp, $sRequest, strlen($sRequest));
       
        $this->sResponse = '';
        while ($sRead = socket_read($sockHttp, 4096))
        {
            $this->sResponse .= $sRead;
        }
       
        socket_close($sockHttp);
    }
   
    function getResponse()
    {
        return $this->sResponse;
                                //echo $this->sResponse;
    }
   
    function getResponseBody()
    {
        $sPatternSeperate = '/\r\n\r\n/';
        $arMatchResponsePart = preg_split($sPatternSeperate, $this->sResponse, 2);
        //return $arMatchResponsePart[1];
                                echo $arMatchResponsePart[1];
    }
}
 
 
 
function DeleteHtml($str){
        $str = trim($str); //除去字串首尾空白
        $str = strip_tags($str,""); //除去html標籤
        $str = ereg_replace("\t","",$str);
        $str = ereg_replace("\r\n","",$str);
        $str = ereg_replace("\r","",$str);
        $str = ereg_replace("\n","",$str);
        $str = ereg_replace(" "," ",$str);
        $str = ereg_replace("&nbsp;"," ",$str);
        return trim($str);
}
 


$sMethod="POST";
$username="簡訊帳號";;
$password="簡訊密碼";
$sPostData=array();

$sPostData[] = array('id'=>'[1]','name'=>'DestName=小明','mobile'=>'dstaddr=0900000001','smsbody'=>'smbody=test LONG SMS');


$content = '';
foreach($sPostData as $v){
	$content .= $v['id']."\n";
	$content .= $v['name']."\n";
	$content .= $v['mobile']."\n";
	$content .= $v['smsbody']."\n";
}
//ECHO $content;
 


$SendGet = new SocketHttpRequest();  
$SendGet->HttpRequest('http://smexpress.mitake.com.tw:9600/SmSendPost.asp?username='.$username.'&password='.$password.'&encoding=utf8');         
$SendGet->sendRequest($sMethod, $content);
$SendGet->getResponseBody();
?>